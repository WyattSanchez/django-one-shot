from django.shortcuts import render, get_object_or_404
from todos.models import TodoList


def show_list(request):
    showlist = TodoList.objects.all()
    context = {"list": showlist}
    return render(request, "todos/list.html", context)


def list_detail(request, id):
    showdetail = get_object_or_404(TodoList, id=id)
    context = {
        "list_detail": showdetail,
    }
    return render(request, "todos/detail.html", context)


# def create_list(request):
#     if request.method == "POST":
