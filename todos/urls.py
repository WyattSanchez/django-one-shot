from django.urls import path
from todos.views import show_list, list_detail


urlpatterns = [
    path("", show_list, name="todo_list_list"),
    path("todos/<int:id>/", list_detail, name="todo_list_detail"),
]
